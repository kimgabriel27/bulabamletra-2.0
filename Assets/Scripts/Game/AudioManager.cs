using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    CurrentWordUIScript WordScript;
    MainGameScript MainScript;
    GameManager gameManager;

    public GameObject TargetWord;
    public AudioSource Audio;
    public AudioClip TargetClip;
    // Start is called before the first frame update
    void Start()
    {
        WordScript = CurrentWordUIScript.Instance;
        MainScript = MainGameScript.GetInstance;
        gameManager = GameManager.instance;
        Audio = gameObject.GetComponent<AudioSource>();
        TargetWord = WordManager.Instance.wordPrefab;
        TargetClip = TargetWord.GetComponent<AudioSource>().clip;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnAudioButtonPress()
    {
      
        Audio.PlayOneShot(TargetClip);
    }
}
