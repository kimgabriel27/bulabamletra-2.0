using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    #region Singleton
    public static GameManager instance;

    //public static GameManager GetInstance
    //{
    //    get
    //    {
    //        if(instance == null)
    //        {
    //            instance = GameObject.FindObjectOfType<GameManager>();
    //        }
    //        return instance;
    //    }
    //}
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            instance = GameObject.FindObjectOfType<GameManager>();
        }
    }
    #endregion

    WordManager WManager;
    PauseMenu PMenu;
    LevelManager levelManager;
    Shooter playerShooter;
    [SerializeField] LineScript lineScript;
    public AudioManager audioManager;

    [SerializeField] private Camera mainCamera;

    public Shooter shootScript;
    public Transform pointerToLastLine;

    private int sequenceSize = 3;
    [SerializeField]
    private List<Transform> bubbleSequence;
    public List<GameObject> TargetWordList;
    public int CWNumber;
    [Header("Word Display")]
    public string TargetWord;
    public string CurrentWord;
    public char[] charTargetWord;

    [Header("UIs ")]
    public GameObject WrongScreen;
    public GameObject LoseScreen;
    public GameObject WinScreen;
    public GameObject blackBG;
    public GameObject loseBG;
    public GameObject ContinueOnClick;
    public GameObject RandomWord;
    public GameObject PanaloUI;
    public Transform SpawnPoint;
    public Transform tutorialCanvas;
    public Button skipButtonGameObject;

    List<GameObject> tutorialImages = new List<GameObject>();
    public bool isTutorial = true;
    int count = 0;
    static int wordCount = 0;

    void Start()
    {
        foreach(Transform t in tutorialCanvas )
        {
            Debug.Log(t.gameObject.name);
            tutorialImages.Add(t.gameObject);
        }

        // disable tutorial when 2 word of the level
        if (wordCount > 0)
        {
            isTutorial = false;
            tutorialCanvas.gameObject.SetActive(false);
        }

        if(!isTutorial)
        {
            audioManager.OnAudioButtonPress();
        }
     
        playerShooter = GameObject.FindGameObjectWithTag("Player").GetComponent<Shooter>();

        bubbleSequence = new List<Transform>();

        levelManager = LevelManager.instance;
        PMenu = PauseMenu.Instance;
        WManager = WordManager.Instance;

        TargetWord = WManager.targetWord;
        charTargetWord = TargetWord.ToCharArray();
        //Debug.Log(LevelManager.instance.gameObject.name);
       // levelManager.GenerateLevel();

        shootScript.canShoot = true;
        shootScript.CreateNextBubble();

        Instantiate(WordManager.Instance.wordPrefab, SpawnPoint.transform.position, Quaternion.identity);
    }

    void Update()
    {
        Vector3 mouseWorldPos = mainCamera.ScreenToWorldPoint(Input.mousePosition);
       
        Debug.Log(mouseWorldPos);
        if (!isTutorial && !PMenu.GamePaused )
        {
            if (shootScript.canShoot && Input.GetMouseButtonDown(0) && (Camera.main.ScreenToWorldPoint(Input.mousePosition).y > -5.0f) && mouseWorldPos.x < 0)
            {
                shootScript.canShoot = false;
                shootScript.Shoot();
            }

            if (shootScript.canShoot
             && Input.GetMouseButtonDown(1)
             && (Camera.main.ScreenToWorldPoint(Input.mousePosition).y > shootScript.transform.position.y) && mouseWorldPos.x < 0)
            {
                shootScript.SwapBubbles();
            }
        }
        else
        {
            if (Input.GetMouseButtonUp(0) && count < 11 )
            {
               
                tutorialImages[count].SetActive(false);
                tutorialImages[count + 1].SetActive(true);
                count++;
            }
            else if(Input.GetMouseButtonUp(0) && count >= 11)
            {
                tutorialImages[count].SetActive(false);
                audioManager.OnAudioButtonPress();
                isTutorial = false;
                skipButtonGameObject.gameObject.SetActive(false);
            }
        }

     
    }

    public void ProcessTurn(Transform currentBubble)
    {
        bubbleSequence.Clear();
        CheckBubbleSequence(currentBubble);

        if (bubbleSequence.Count >= sequenceSize)
        {
            CurrentWord += currentBubble.GetComponent<Bubble>().bubbleLetter.ToString();
            DestroyBubblesInSequence();
            DropDisconectedBubbles();

            //List<GameObject> objectBubbles = new List<GameObject>();

            ////foreach (Transform t in levelManager.bubblesArea)
            ////{

            ////    objectBubbles.Add(t.gameObject);

            ////}

            ////if (objectBubbles.Count <= 25)
            ////{
            ////    levelManager.AddNewLine();
            ////}

            //objectBubbles.Clear();
        }
     

        List<GameObject> objects = new List<GameObject>();

        foreach (Transform t in levelManager.bubblesArea)
        {

            objects.Add(t.gameObject);

        }

        if (objects.Count <= 15)
        {
            levelManager.AddNewLine();
        }

        objects.Clear();
        LevelManager.instance.UpdateListOfBubblesInScene();

        shootScript.CreateNextBubble();
        shootScript.canShoot = true;
    }

    private void CheckBubbleSequence(Transform currentBubble)
    {
        bubbleSequence.Add(currentBubble);

        Bubble bubbleScript = currentBubble.GetComponent<Bubble>();
        List<Transform> neighbors = bubbleScript.GetNeighbors();

        foreach (Transform t in neighbors)
        {
            if (!bubbleSequence.Contains(t))
            {

                Bubble bScript = t.GetComponent<Bubble>();

                if (bScript.bubbleLetter == bubbleScript.bubbleLetter)
                {
                    CheckBubbleSequence(t);
                }
            }
        }
    }

    private void DestroyBubblesInSequence()
    {
        foreach (Transform t in bubbleSequence)
        {
            t.gameObject.GetComponent<Bubble>().isDeletingSelf = true;
            float i = t.gameObject.GetComponent<Bubble>().BubbleDestroy();
         //   Destroy(t.gameObject);
          
            //Destroy(t.gameObject);
        }
    }

    private void DropDisconectedBubbles()
    {
        SetAllBubblesConnectionToFalse();
        SetConnectedBubblesToTrue();
        SetGravityToDisconectedBubbles();
    }

    #region Drop Disconected Bubbles
    private void SetAllBubblesConnectionToFalse()
    {
        Debug.Log("Connection False");
        foreach (Transform bubble in LevelManager.instance.bubblesArea)
        {
            bubble.GetComponent<Bubble>().isConnected = false;
        }
    }

    private void SetConnectedBubblesToTrue()
    {
        bubbleSequence.Clear();

      //  Debug.DrawRay(pointerToLastLine.position, pointerToLastLine.right * 15f, Color.red);
        RaycastHit2D[] hits = Physics2D.RaycastAll(pointerToLastLine.position, pointerToLastLine.right, 500f);

        for (int i = 0; i < hits.Length; i++)
        {
          //  Debug.Log(hits[i].transform.gameObject.name + "Position: " + hits[i].transform.position);
            if (hits[i].transform.gameObject.tag.Equals("Bubble") )
                SetNeighboursConnectionToTrue(hits[i].transform);
        }
    }

    private void SetNeighboursConnectionToTrue(Transform bubble)
    {
        Bubble bubbleScript = bubble.GetComponent<Bubble>();
        bubbleScript.isConnected = true;
        if (bubble != null )
            bubbleSequence.Add(bubble);
           
            

        foreach (Transform t in bubbleScript.GetNeighbors())
        {
            if (!bubbleSequence.Contains(t))
            {
                SetNeighboursConnectionToTrue(t);
            }
        }
    }

    private void SetGravityToDisconectedBubbles()
    {   
        foreach (Transform bubble in LevelManager.instance.bubblesArea)
        {
            if (!bubble.GetComponent<Bubble>().isConnected)
            {
                Debug.Log("Adding Rigidbody");
                bubble.gameObject.GetComponent<CircleCollider2D>().enabled = false;
                if (!bubble.GetComponent<Rigidbody2D>())
                {
                    Rigidbody2D rb2d = bubble.gameObject.AddComponent(typeof(Rigidbody2D)) as Rigidbody2D;
                    rb2d.gravityScale = 1;
                }

                bubble.GetComponent<Bubble>().DroppingDestroy();
            }
        }
    }
    #endregion

    public void ClearButton()
    {
        CurrentWord = "";
    }

    public void EraseButton()
    {
        if(CurrentWord.Length > 0)
        {
            CurrentWord = CurrentWord.Remove(CurrentWord.Length - 1);
        }
    
    }

    public bool CheckWordsForMistakes()
    {
        if (CurrentWord == "")
        {
            return true;
        }

        if (CurrentWord != TargetWord)
        {

            return true;
        }

        return false;
    }

    public void ShowLoseScreen()
    {
        LoseScreen.SetActive(true);
        loseBG.SetActive(true);
        LoseScreen.GetComponent<AudioSource>().Play();
        wordCount = 0;
        ShooterDisable(false);
        //ContinueOnClick.SetActive(true);
    }

    public void ShowWinScreen()
    {

        if (CheckWordsForMistakes())
        {
            StartCoroutine(OnWrongWord());
            //GameEvents.current.WrongWord();
            CurrentWord = "";

        }
        else
        {
            Debug.Log("Words Remaining:" + WManager.GetWordListCount());
            wordCount++; 
            if (WManager.GetWordListCount() > 0)
            {

                Debug.Log("Next Level");
                StartCoroutine(CorrectScreen("MainGame"));
                
      

                GameObject v = RandomWord.gameObject;
                Destroy(v);

                // GameEvents.current.LevelReset();

            }
            else if (WManager.GetWordListCount() <= 0 || wordCount  >= 5 )
            {
                Debug.Log("Go To mainMenu");
                wordCount = 0;
                isTutorial = true;

                StartCoroutine(Panalo());
               // StartCoroutine(GoToMainMenu("MainMenu"));

                //GameObject v = RandomWord.gameObject;
                //Destroy(v);
            }

        }

    }

    private void GetRandomWords() // words per difficulty
    {
        int N = WManager.GetComponent<WordManager>().Words.Count;
        int RandomWordNumber = Random.Range(0, N - 1);


        switch (WManager.GetWordDifficulty())
        {
            case 0:

                Debug.Log("easy");

                RandomWordNumber = Random.Range(0, WManager.GetComponent<WordManager>().EWords.Count);

                RandomWord = WManager.GetComponent<WordManager>().EWords[RandomWordNumber];
                TargetWord = WManager.GetComponent<WordManager>().EWords[RandomWordNumber].name;
                foreach (var i in WManager.EWords)
                {
                    TargetWordList.Add(i);
                }
                TargetWordList.Remove(WManager.GetComponent<WordManager>().EWords[RandomWordNumber]);

                break;

            case 1:
                Debug.Log("ave");

                RandomWordNumber = Random.Range(0, WManager.GetComponent<WordManager>().MWords.Count);
                RandomWord = WManager.GetComponent<WordManager>().MWords[RandomWordNumber];
                TargetWord = WManager.GetComponent<WordManager>().MWords[RandomWordNumber].name;
                foreach (var i in WManager.EWords)
                {
                    TargetWordList.Add(i);
                }
                TargetWordList.Remove(WManager.GetComponent<WordManager>().MWords[RandomWordNumber]);

                break;

            case 2:
                Debug.Log("hard");
                //RandomNumber = Random.Range(0, WManager.HardWords.Count);
                //TargetWord.Add(WManager.HardWords[RandomNumber]);
                RandomWordNumber = Random.Range(0, WManager.GetComponent<WordManager>().HWords.Count);
                RandomWord = WManager.GetComponent<WordManager>().HWords[RandomWordNumber];
                TargetWord = WManager.GetComponent<WordManager>().HWords[RandomWordNumber].name;
                foreach (var i in WManager.EWords)
                {
                    TargetWordList.Add(i);
                }
                TargetWordList.Remove(RandomWord = WManager.GetComponent<WordManager>().HWords[RandomWordNumber]);


                //  WManager.RemoveWordFromList(RandomNumber);
                break;

            default:
                break;
        }

    }
    
    public void SkipTutorialButtonClicked()
    {
        isTutorial = false;
        foreach(GameObject go in tutorialImages)
        {
            go.SetActive(false);

        }
        skipButtonGameObject.gameObject.SetActive(false);
    }
    
    IEnumerator CorrectScreen(string sceneName)
    {
        playerShooter.canShoot = false;
        WinScreen.SetActive(true);
        WinScreen.GetComponent<AudioSource>().Play();
        blackBG.SetActive(true);
        ContinueOnClick.SetActive(true);
        yield return new WaitForSeconds(.8f);

        WinScreen.SetActive(false);
        blackBG.SetActive(false);
        ContinueOnClick.SetActive(false);
        playerShooter.canShoot = true;
        WManager.ChooseAnotherWord();
   
        SceneManager.LoadScene(sceneName);

    }

    IEnumerator GoToMainMenu(string sceneName)
    {
        playerShooter.canShoot = false;
        WinScreen.SetActive(true);
        WinScreen.GetComponent<AudioSource>().Play();
        blackBG.SetActive(true);
        ContinueOnClick.SetActive(true);
        yield return new WaitForSeconds(.8f);

        WinScreen.SetActive(false);
        blackBG.SetActive(false);
        ContinueOnClick.SetActive(false);
        playerShooter.canShoot = true;
        //levelManager.assignedLevelDesigns = null;
        levelManager.SetNumber();
        levelManager.ListClear();
        SceneManager.LoadScene(sceneName);
    }

    IEnumerator OnWrongWord()
    {
        levelManager.AddNewLine();  
        WrongScreen.SetActive(true);
        blackBG.SetActive(true);
        playerShooter.canShoot = false;
        lineScript.LineActive(false);
        yield return new WaitForSeconds(1f);
        playerShooter.canShoot = true;
        blackBG.SetActive(false);
        WrongScreen.SetActive(false);
        lineScript.LineActive(true);
    }

    IEnumerator WaitForDeletion()
    {

        yield return new WaitForSeconds(2);
    }

    IEnumerator Panalo()
    {
        playerShooter.canShoot = false;
        WinScreen.SetActive(true);
        WinScreen.GetComponent<AudioSource>().Play();
        blackBG.SetActive(true);
        ContinueOnClick.SetActive(true);
        yield return new WaitForSeconds(.8f);

        WinScreen.SetActive(false);
        blackBG.SetActive(false);
        ContinueOnClick.SetActive(false);
        PanaloUI.SetActive(true);
        PanaloUI.GetComponent<AudioSource>().Play();
    }

    public  void ShooterDisable(bool isDisable)
    {
        shootScript.canShoot = isDisable;
    }
}
