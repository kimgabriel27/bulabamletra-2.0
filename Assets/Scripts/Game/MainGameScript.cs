using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.SceneManagement;

public class MainGameScript : MonoBehaviour
{
    MainGameScript() { }

    public static MainGameScript GetInstance
    {
        get
        {
            if (Instance == null)
            {
                Instance = GameObject.FindObjectOfType<MainGameScript>();
                Debug.Log("Instance Made");
            }
            return Instance;
        }
    }
     
    private static MainGameScript Instance;
    public static int CurrentScore = 0;

    ObjectPoolScript LetterPool;
    WordManager WManager;
    PauseMenu PMenu;
    private PlayerScript player;

    //Public Variables
    [Header("Word Display")]
    public string TargetWord;
    public string CurrentWord;


    public static bool SettingsActive = false;
    public GameObject SettingsUI;
    public int MaxCWNumber;
    public int CWNumber;

    [Header("UIs ")]
    public GameObject WrongScreen;
    public GameObject LoseScreen;
    public GameObject WinScreen;
    public GameObject blackBG;
    public GameObject ContinueOnClick;
    public GameObject RandomWord;
    public Transform SpawnPoint;

    //GameObjects of CurrentWords
    public GameObject WordObjects;
    public GameObject Player;

    //GridSpawning Variables
    [Header("Grid Spawning Variables")]
    public GameObject spawnThis;
    public int x = 5;
    public int y = 5;
    public float XAdjustment;
    public float YAdjustment;
    public float radius = 0.5f;
    public bool useAsInnerCircleRadius = true;

    //Private Variables
    private string Word = "";
    private float offsetX, offsetY;

    [Header("Lists")]
    public char[] LetterArray;
    public List<string> LetterList;
    public List<int> RandomNumbers;
    public List<GameObject> TargetWordList;

    //configurable number
    public int AmountOfLetters;
    int LineCounter;
    public int SpawnerCounter;
    int LetterCounter;

    public PlayerScript playerScript;
  
    private void Start()
    {
        Instance = this;
        LetterPool = ObjectPoolScript.Instance;
        WManager = WordManager.Instance;
        PMenu = PauseMenu.Instance;
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerScript>();
        //GameEvents.current.OnLetterSpawn += SpawnLetter;
        //GameEvents.current.OnLetterScoreEvent += ScoreLetter;   
      
        CurrentScore = 0;
        CWNumber = 0;
        LineCounter = 0;    
        SpawnerCounter = 0;

        NewList();
        //GetRandomWords();

        GameEvents.current.LoadText();

        TargetWord = WordManager.Instance.targetWord;
        LetterArray = WordManager.Instance.targetWord.ToCharArray();

        Player.GetComponent<PlayerScript>().PlayerLetterArray = LetterArray;

        playerScript.Load();

        Instantiate(WordManager.Instance.wordPrefab, SpawnPoint.transform.position, Quaternion.identity);
    }

    // Update is called once per frame
    void Update()
    {
        //if (CurrentWord == TargetWord)
        //{
        //    ShowWinScreen();
        //}
    }

    void NewList()
    {
        for (int i = 0; i < AmountOfLetters; i++)
        {
            RandomNumbers.Add(Random.Range(0, LetterList.Count - 1));
        }

        RandomNumbers.Distinct().ToList();

        Player.GetComponent<PlayerScript>().PlayerRandomNumbers = RandomNumbers;
    }

    public void ClearButton()
    {
        CurrentWord = "";
    }

    public void EraseButton()
    {
        CurrentWord = CurrentWord.Remove(CurrentWord.Length - 1);
    }

    public void ShowWinScreen()
    {
       
        if (CheckWordsForMistakes())
        {
            ///LoseScreen.SetActive(true);
            GameEvents.current.WrongWord();
            CurrentWord = "";
         
        }
        else
        {
       
            if(WManager.GetWordListCount() > 0)
            {

           
                StartCoroutine(CorrectScreen());
                GameObject v = RandomWord.gameObject;
                 Destroy(v);
         

            
               // GameEvents.current.LevelReset();

            }
            else
            {
                
                PMenu.GamePaused = true;
                WinScreen.SetActive(true);
                WinScreen.GetComponent<AudioSource>().Play();
                blackBG.SetActive(true);
                ContinueOnClick.SetActive(true);
            }
        
        }
    
    }

    public void ShowLoseScreen()
    {
        LoseScreen.SetActive(true);
        LoseScreen.GetComponent<AudioSource>().Play();
        //ContinueOnClick.SetActive(true);
    }

    public bool CheckWordsForMistakes()
    {
       
        if (CurrentWord == "")
        {
            StartCoroutine(ShowMaliScreen());
            return true;
        }

        if (CurrentWord != TargetWord)
        {
            StartCoroutine(ShowMaliScreen());
            return true;
        }

        return false;
    }

    private void GetRandomWords() // words per difficulty
    {
        int N = WManager.GetComponent<WordManager>().Words.Count;
        int RandomWordNumber = Random.Range(0, N - 1);

       
        switch (WManager.GetWordDifficulty())
        {
            case 0:
               
                Debug.Log("easy");

                RandomWordNumber = Random.Range(0, WManager.GetComponent<WordManager>().EWords.Count);

                RandomWord = WManager.GetComponent<WordManager>().EWords[RandomWordNumber];
                TargetWord = WManager.GetComponent<WordManager>().EWords[RandomWordNumber].name;
                foreach (var i in WManager.EWords)
                {
                    TargetWordList.Add(i);
                }
                TargetWordList.Remove(WManager.GetComponent<WordManager>().EWords[RandomWordNumber]);
              

                break;

            case 1:
                Debug.Log("ave");


                RandomWordNumber = Random.Range(0, WManager.GetComponent<WordManager>().MWords.Count);
                RandomWord = WManager.GetComponent<WordManager>().MWords[RandomWordNumber];
                TargetWord = WManager.GetComponent<WordManager>().MWords[RandomWordNumber].name;
                foreach (var i in WManager.EWords)
                {
                    TargetWordList.Add(i);
                }
                TargetWordList.Remove(WManager.GetComponent<WordManager>().MWords[RandomWordNumber]);

                //WManager.RemoveWordFromList(RandomNumber);
                break;

            case 2:
                Debug.Log("hard");


                RandomWordNumber = Random.Range(0, WManager.GetComponent<WordManager>().HWords.Count);
                RandomWord = WManager.GetComponent<WordManager>().HWords[RandomWordNumber];
                TargetWord = WManager.GetComponent<WordManager>().HWords[RandomWordNumber].name;
                foreach (var i in WManager.EWords)
                {
                    TargetWordList.Add(i);
                }
                TargetWordList.Remove(RandomWord = WManager.GetComponent<WordManager>().HWords[RandomWordNumber]);


                break;

            default:
                break;
        }


      
       // int RandomWordNumber = Random.Range(0, N - 1);

        // RandomWord = WManager.GetComponent<WordManager>().Words[RandomWordNumber];
        //TargetWord = WManager.GetComponent<WordManager>().Words[RandomWordNumber].name;
    }

    private void ScoreLetter()
    {
        CurrentScore++;
    }

    Vector2 HexOffset(float x, float y)
    {
        x += XAdjustment;
        y += YAdjustment;

        Vector2 position = Vector2.zero;

        if (y % 2 == 0)
        {
            position.x = x * offsetX;
            position.y = y * offsetY;
        }
        else
        {
            position.x = (x + 0.5f) * offsetX;
            position.y = y * offsetY;
        }

        return position;
    }

    private void SpawnLetter(GameObject Spawn) //
    {
        //abtedilu

        //y = 30
        //int counter
        //int LIN = letters in word
        //int line counter = 30/LIN
        //if word[counter] == "A" then 

        
        Debug.Log("Spawning letter");
        int SpawnerRandomNumber;
        SpawnerRandomNumber = Random.Range(0,100);

       
        if(SpawnerRandomNumber >= 95)
        {
            for (int i = 0; i < TargetWord.Length; i++)
            {
                char c = TargetWord[i];
                LetterPool.SpawnFromPool(c.ToString(), Spawn.transform.position, Quaternion.identity);
                SpawnerCounter++;
            }
            //LetterPool.SpawnFromPool(LetterArray[LineCounter].ToString(), Spawn.transform.position, Quaternion.identity);
            SpawnerCounter++;
        }
        else
        {
            //if (SpawnerRandomNumber >= 50)
            //{
            //    char c = TargetWord[Random.Range(0, TargetWord.Length)];
            //    LetterPool.SpawnFromPool(c.ToString(), Spawn.transform.position, Quaternion.identity);
            //    SpawnerCounter++;
            //}
            //else
            //{
            //    LetterPool.SpawnFromPool(LetterList[RandomNumbers[Random.Range(0, RandomNumbers.Count)]], Spawn.transform.position, Quaternion.identity);
            //    SpawnerCounter++;
            //}
            char c = TargetWord[Random.Range(0, TargetWord.Length)];
            LetterPool.SpawnFromPool(c.ToString(), Spawn.transform.position, Quaternion.identity);
            SpawnerCounter++;
            //LetterPool.SpawnFromPool(LetterList[RandomNumbers[Random.Range(0, RandomNumbers.Count)]], Spawn.transform.position, Quaternion.identity);
        }

        if (SpawnerCounter >= x * 3)
        {
            LineCounter++;
        }

        //if (LineCounter > LetterArray.Length - 1)
        //{
        //    LineCounter = 0;
        //}
    }

    IEnumerator ShowMaliScreen()
    {
        player.canShoot = false;
        WrongScreen.SetActive(true);
        WrongScreen.GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(1);
        WrongScreen.SetActive(false);
        player.canShoot = true;
    }

    IEnumerator CorrectScreen()
    {
        player.canShoot = false;
        WinScreen.SetActive(true);
        WinScreen.GetComponent<AudioSource>().Play();
        blackBG.SetActive(true);
        ContinueOnClick.SetActive(true);
        yield return new WaitForSeconds(2);


        WinScreen.SetActive(false);
        blackBG.SetActive(false);
        ContinueOnClick.SetActive(false );
        player.canShoot = true;
        WManager.ChooseAnotherWord();
        SceneManager.LoadScene("MainGame");

    }
}
