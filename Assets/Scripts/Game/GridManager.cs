using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Collections;

public class GridManager : MonoBehaviour
{
    [System.Serializable]
    public class Letters
    {
        public string tag;
        public GameObject LetterPrefab;
    }

    public GameObject bubble;
    public Vector3 initPos2;
    public Vector3 initPos;
    public int columns;
    public int rows;
    public GameObject[,] grid;
    public float gap;

    public List<int> TakeList = new List<int>();
    public List<string> lettersTW = new List<string>();


    private int randNum;
    int lastNumber;
    int lastlastNum;
    bool foundNum = false;
    char newLetter;

    [SerializeField] private char[] TargetWordArray;
    [SerializeField] private List<Letters> LetterList;
    public Dictionary<string, GameObject> LetterCollection = new Dictionary<string, GameObject>();


    [SerializeField] private MainGameScript mainGameScript;
    const int COL_MAX = 12;
    const int ROW_MAX = 20;
    int rw = 1;

    // Start is called before the first frame update
    void Start()
    {
        TakeList = new List<int>(new int[TargetWordArray.Length]);
        LetterCollection = new Dictionary<string, GameObject>();
        mainGameScript = this.gameObject.GetComponent<MainGameScript>();
        TargetWordArray = mainGameScript.LetterArray;
        grid = new GameObject[COL_MAX, ROW_MAX];

        foreach (Letters letters in LetterList)
        {
            LetterCollection.Add(letters.tag, letters.LetterPrefab);
        }

        for (int r = 0; r < rows; r++)
        {
            if (r % 2 != 0) columns -= 1;
            for (int c = 0; c < columns; c++)
            {
                Vector3 position = new Vector3((float)c * gap, (float)(-r) * gap, 0f) + initPos;
                if (r % 2 != 0)
                    position.x += 0.5f * gap;

                if (TakeList.Count == 0)
                {
                    for (int i = 0; i < TargetWordArray.Length; i++)
                    {
                        TakeList.Add(i);

                    }
                    for (int i = TakeList.Count - 1; i > 0; i--)
                    {
                        // Randomize a number between 0 and i (so that the range decreases each time)
                        int rnd = UnityEngine.Random.Range(0, i);

                        // Save the value of the current i, otherwise it'll overwrite when we swap the values
                        int temp = TakeList[i];

                        // Swap the new and old values
                        TakeList[i] = TakeList[rnd];
                        TakeList[rnd] = temp;
                    }
                    // Shuffle(TakeList);

                }

                newLetter = TargetWordArray[TakeList[0]]; //Need tag
                Create(position, newLetter.ToString());
                Debug.Log(newLetter);
                TakeList.RemoveAt(0);


            }
            if (r % 2 != 0) columns += 1;
        }
    }

    public Vector3 Snap(Vector3 position)
    {
        Vector3 objectOffset = position - initPos;
        Vector3 objectSnap = new Vector3(
            Mathf.Round(objectOffset.x / gap),
            Mathf.Round(objectOffset.y / gap),
            0f
        );

        if ((int)objectSnap.y % 2 != 0)
        {
            if (objectOffset.x > objectSnap.x * gap)
            {
                objectSnap.x += 0.5f;
            }
            else
            {
                objectSnap.x -= 0.5f;
            }
        }
        // Debug.Log(initPos + objectSnap * gap);
        return initPos + objectSnap * gap;
    }

    public GameObject Create(Vector2 position, string tag)
    {

        Vector3 snappedPosition = Snap(position);
        int row = (int)Mathf.Round((snappedPosition.y - initPos.y) / gap);
        int column = 0;
        if (row % 2 != 0)
        {
            column = (int)Mathf.Round((snappedPosition.x - initPos.x) / gap - 0.5f);
        }
        else
        {
            column = (int)Mathf.Round((snappedPosition.x - initPos.x) / gap);
        }

        if (!LetterCollection.ContainsKey(tag))
        {
            return null;
        }

        GameObject bubbleClone = (GameObject)Instantiate(LetterCollection[tag], snappedPosition, Quaternion.identity);

        try
        {
            grid[column, -row] = bubbleClone;
        }
        catch (System.IndexOutOfRangeException)
        {
            Destroy(bubbleClone);
            Debug.Log("IndexOutOfRangeException");
            return null;
        }

        CircleCollider2D collider = bubbleClone.GetComponent<CircleCollider2D>();
        if (collider != null)
        {
            collider.isTrigger = true;
        }

        GridMember gridMember = bubbleClone.GetComponent<GridMember>();
        if (gridMember != null)
        {

            gridMember.parent = gameObject;
            gridMember.row = row;
            gridMember.column = column;

            SpriteRenderer spriteRenderer = bubbleClone.GetComponent<SpriteRenderer>();
            //if (spriteRenderer != null)
            //{
            //    Color[] colorArray = new Color[] { Color.red, Color.cyan, Color.yellow, Color.green, Color.magenta };
            //    spriteRenderer.color = colorArray[gridMember.kind - 1];
            //}
        }
        bubbleClone.SetActive(true);

        //if (column == 6 && row == -6 && youLose != null)
        //    youLose.SetActive(true);

        try
        {
            grid[column, -row] = bubbleClone;
        }
        catch (System.IndexOutOfRangeException)
        {
        }

        return bubbleClone;
    }

    public void Seek(int column, int row, string kind)
    {
        int[] pair = new int[2] { column, row };

        bool[,] visited = new bool[COL_MAX, ROW_MAX];

        visited[column, row] = true;

        int[] deltax = { -1, 0, -1, 0, -1, 1 };
        int[] deltaxprime = { 1, 0, 1, 0, -1, 1 };
        int[] deltay = { -1, -1, 1, 1, 0, 0 };


        Queue<int[]> queue = new Queue<int[]>();
        Queue<GameObject> objectQueue = new Queue<GameObject>();

        queue.Enqueue(pair);

        int count = 0;
        while (queue.Count != 0)
        {
            int[] top = queue.Dequeue();
            GameObject gtop = grid[top[0], top[1]];
            if (gtop != null)
            {
                objectQueue.Enqueue(gtop);
            }
            count += 1;
            for (int i = 0; i < 6; i++)
            {
                int[] neighbor = new int[2];
                if (top[1] % 2 == 0)
                {
                    neighbor[0] = top[0] + deltax[i];
                }
                else
                {
                    neighbor[0] = top[0] + deltaxprime[i];
                }
                neighbor[1] = top[1] + deltay[i];
                try
                {
                    GameObject g = grid[neighbor[0], neighbor[1]];
                    if (g != null)
                    {
                        GridMember gridMember = g.GetComponent<GridMember>();
                        if (gridMember != null && gridMember.kind == kind) // if Same kind 
                        {
                            if (!visited[neighbor[0], neighbor[1]])
                            {
                                visited[neighbor[0], neighbor[1]] = true;
                                queue.Enqueue(neighbor);
                            }
                        }
                    }
                }
                catch (System.IndexOutOfRangeException)
                {
                }
            }
        }
        if (count >= 3) // IF the same kind is three or more
        {
            while (objectQueue.Count != 0)
            {
                GameObject g = objectQueue.Dequeue();

                GridMember gm = g.GetComponent<GridMember>();
                if (gm != null)
                {
                    grid[gm.column, -gm.row] = null;
                    gm.state = "Pop";
                }
            }
            mainGameScript.CurrentWord += kind; // Add Letter
            AudioSource audioSource = GetComponent<AudioSource>();
            if (audioSource != null)
                audioSource.Play();
        }
        CheckCeiling(0);
    }

    public void CheckCeiling(int ceiling)
    {

        bool[,] visited = new bool[COL_MAX, ROW_MAX];

        Queue<int[]> queue = new Queue<int[]>();

        int[] deltax = { -1, 0, -1, 0, -1, 1 };
        int[] deltaxprime = { 1, 0, 1, 0, -1, 1 };
        int[] deltay = { -1, -1, 1, 1, 0, 0 };

        for (int i = 0; i < COL_MAX; i++)
        {
            int[] pair = new int[2] { i, ceiling };
            if (grid[i, ceiling] != null)
            {

                visited[i, ceiling] = true;
                queue.Enqueue(pair);
            }
        }

        int count = 0;
        while (queue.Count != 0)
        {
            int[] top = queue.Dequeue();
            count += 1;
            for (int i = 0; i < 6; i++)
            {
                int[] neighbor = new int[2];
                if (top[1] % 2 == 0)
                {
                    neighbor[0] = top[0] + deltax[i];
                }
                else
                {
                    neighbor[0] = top[0] + deltaxprime[i];
                }
                neighbor[1] = top[1] + deltay[i];
                try
                {
                    GameObject g = grid[neighbor[0], neighbor[1]];
                    if (g != null)
                    {

                        if (!visited[neighbor[0], neighbor[1]])
                        {

                            visited[neighbor[0], neighbor[1]] = true;
                            queue.Enqueue(neighbor);
                        }
                    }
                }
                catch (System.IndexOutOfRangeException)
                {
                }
            }
        }

        for (int r = 0; r < ROW_MAX; r++)
        {
            for (int c = 0; c < COL_MAX; c++)
            {
                if (grid[c, r] != null && !visited[c, r])
                {
                    GameObject g = grid[c, r];
                    GridMember gm = g.GetComponent<GridMember>();
                    if (gm != null)
                    {
                        grid[gm.column, -gm.row] = null;
                        gm.state = "Explode";
                    }
                }
            }
        }

        if (count <= 0)
        {
            Debug.Log(count);
            NewSetBubbles();
            //AdjustGridMembers(0);
            //      newSetBubbles();
            //if (youWin != null)
            //    youWin.SetActive(true);
        }

    }

    int CheckForRepeatingLetter(int num)
    {
        for (int i = 0; i < TakeList.Count; i++)
        {
            if (TargetWordArray[num].ToString() == TargetWordArray[TakeList[i]].ToString())
            {
                Debug.Log(TargetWordArray[TakeList[i]].ToString());
                foundNum = true;
                lastNumber = i;
                break;
            }

        }
        return lastNumber;
    }

    void Shuffle(List<int> a)
    {
        // Loop array
        for (int i = a.Count - 1; i > 0; i--)
        {
            // Randomize a number between 0 and i (so that the range decreases each time)
            int rnd = UnityEngine.Random.Range(0, i);

            // Save the value of the current i, otherwise it'll overwrite when we swap the values
            int temp = a[i];

            // Swap the new and old values
            a[i] = a[rnd];
            a[rnd] = temp;
        }



    }

    //void AddRowLetters(int newColumn)
    //{
    //    Debug.Log("Adding Row Letters");

    //    if (rw % 2 != 0) columns -= 1;
    //    if (rw % 2 != 0) columns += 1;
    //    for (int c = 0; c < columns; c++)
    //    {
    //        Vector3 position = new Vector3((float)c * gap, (float)(-rw) * gap, 0f) + initPos;
    //        if (rw % 2 != 0)
    //            position.x += 0.5f * gap;

    //        char newLetter;


    //        if (TakeList.Count == 0)
    //        {
    //            for (int i = 0; i < TargetWordArray.Length; i++)
    //            {
    //                TakeList.Add(i);

    //            }
    //            for (int i = TakeList.Count - 1; i > 0; i--)
    //            {
    //                // Randomize a number between 0 and i (so that the range decreases each time)
    //                int rnd = UnityEngine.Random.Range(0, i);

    //                // Save the value of the current i, otherwise it'll overwrite when we swap the values
    //                int temp = TakeList[i];

    //                // Swap the new and old values
    //                TakeList[i] = TakeList[rnd];
    //                TakeList[rnd] = temp;
    //            }
    //            // Shuffle(TakeList);

    //        }

    //        newLetter = TargetWordArray[TakeList[0]]; //Need tag
    //        Create(position, newLetter.ToString());
    //        Debug.Log(newLetter);
    //        TakeList.RemoveAt(0);

    //    }
    //    rw++;

    //}

    public void AdjustGridMembers(int ceiling)
    {
        Debug.Log("AddRow of Letters");
        Debug.Log("Check Ceiling");

        bool[,] visited = new bool[COL_MAX, ROW_MAX];

        Queue<int[]> queue = new Queue<int[]>();

        int[] deltax = { -1, 0, -1, 0, -1, 1 };
        int[] deltaxprime = { 1, 0, 1, 0, -1, 1 };
        int[] deltay = { -1, -1, 1, 1, 0, 0 };

        for (int i = 0; i < COL_MAX; i++)
        {
            int[] pair = new int[2] { i, ceiling };
            if (grid[i, ceiling] != null)
            {
                visited[i, ceiling] = true;
                queue.Enqueue(pair);
            }
        }

        int count = 0;
        while (queue.Count != 0)
        {
            int[] top = queue.Dequeue();
            count += 1;
            for (int i = 0; i < 6; i++)
            {
                int[] neighbor = new int[2];
                if (top[1] % 2 == 0)
                {
                    neighbor[0] = top[0] + deltax[i];
                }
                else
                {
                    neighbor[0] = top[0] + deltaxprime[i];
                }
                neighbor[1] = top[1] + deltay[i];
                try
                {
                    GameObject g = grid[neighbor[0], neighbor[1]];
                    if (g != null)
                    {
                        Debug.Log(g + " = row:" + g.GetComponent<GridMember>().row + " column: " + g.GetComponent<GridMember>().column);
                        if (!visited[neighbor[0], neighbor[1]])
                        {
                            visited[neighbor[0], neighbor[1]] = true;
                            queue.Enqueue(neighbor);
                        }
                        //  g.GetComponent<GridMember>().row -= 1;
                        //AdjustSnap(g);
                        //Snap(g.transform.position + g.GetComponent<GridMember>().differentialPos);
                        //g.GetComponent
                    }
                }
                catch (System.IndexOutOfRangeException)
                {
                }

            }

        }
        //GameEvents.current.LetterMove();
        //AddRowLetters(7);
    }

    //void NewCreateRow(Vector2 position, string tag)
    //{
    //    // separate
    //    Vector3 snappedPosition = Snap(position);
    //    int row = (int)Mathf.Round((snappedPosition.y - initPos.y) / gap);
    //    int column = 0;
    //    if (row % 2 != 0)
    //    {
    //        column = (int)Mathf.Round((snappedPosition.x - initPos.x) / gap - 0.5f);
    //    }
    //    else
    //    {
    //        column = (int)Mathf.Round((snappedPosition.x - initPos.x) / gap);
    //    }



    //    //GameObject bubbleClone = (GameObject)Instantiate(LetterCollection[tag], snappedPosition, Quaternion.identity);

    //    //try
    //    //{
    //    //    grid[column, -row] = bubbleClone;
    //    //}
    //    //catch (System.IndexOutOfRangeException)
    //    //{
    //    //    Destroy(bubbleClone);
    //    //    return null;
    //    //}

    //    //CircleCollider2D collider = bubbleClone.GetComponent<CircleCollider2D>();
    //    //if (collider != null)
    //    //{
    //    //    collider.isTrigger = true;
    //    //}

    //    //GridMember gridMember = bubbleClone.GetComponent<GridMember>();
    //    //if (gridMember != null)
    //    //{

    //    //    gridMember.parent = gameObject;
    //    //    gridMember.row = row;
    //    //    gridMember.column = column;


    //    //}
    //    //bubbleClone.SetActive(true);

    //    //try
    //    //{
    //    //    grid[column, -row] = bubbleClone;
    //    //}
    //    //catch (System.IndexOutOfRangeException)
    //    //{
    //    //}

    //    //return bubbleClone;
    //}

    //void AdjustSnap(GameObject g)
    //{
    //    Vector3 objectOffset = (g.transform.position + g.GetComponent<GridMember>().differentialPos ) -  initPos;
    //    Vector3 objectSnap = new Vector3(
    //        Mathf.Round(objectOffset.x / gap),
    //        Mathf.Round(objectOffset.y / gap),
    //        0f
    //    );

    //    if ((int)objectSnap.y % 2 != 0)
    //    {
    //        if (objectOffset.x > objectSnap.x * gap)
    //        {
    //            objectSnap.x += 0.5f;
    //        }
    //        else
    //        {
    //            objectSnap.x -= 0.5f;
    //        }
    //    }
    //    NewCreateRow(initPos + objectSnap * gap, g.GetComponent<GridMember>().kind);
    //}    

    //void AddList(List<int> a)
    //{
    //    for (int i = 0; i < TargetWordArray.Length; i++)
    //    {
    //        a.Add(i);

    //    }

    //    Shuffle(a);
    //}

    void NewSetBubbles()
    {
        grid = new GameObject[COL_MAX, ROW_MAX];


        for (int r = 0; r < rows; r++)
        {
            if (r % 2 != 0) columns -= 1;
            for (int c = 0; c < columns; c++)
            {
                Vector3 position = new Vector3((float)c * gap, (float)(-r) * gap, 0f) + initPos;
                if (r % 2 != 0)
                    position.x += 0.5f * gap;

                if (TakeList.Count == 0)
                {
                    for (int i = 0; i < TargetWordArray.Length; i++)
                    {
                        TakeList.Add(i);

                    }
                    for (int i = TakeList.Count - 1; i > 0; i--)
                    {
                        // Randomize a number between 0 and i (so that the range decreases each time)
                        int rnd = UnityEngine.Random.Range(0, i);

                        // Save the value of the current i, otherwise it'll overwrite when we swap the values
                        int temp = TakeList[i];

                        // Swap the new and old values
                        TakeList[i] = TakeList[rnd];
                        TakeList[rnd] = temp;
                    }
                    // Shuffle(TakeList);

                }

                newLetter = TargetWordArray[TakeList[0]]; //Need tag
                Create(position, newLetter.ToString());
                Debug.Log(newLetter);
                TakeList.RemoveAt(0);


            }
            if (r % 2 != 0) columns += 1;
        }
    }
}