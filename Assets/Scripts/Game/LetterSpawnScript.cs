using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LetterSpawnScript : MonoBehaviour
{
    public Letter numberLetter;
    // Start is called before the first frame update
    void Start()
    {
        GameEvents.current.SpawnLetter(gameObject);
        gameObject.SetActive(false);
    }

    public enum Letter
    {
        A = 0,
        B = 1, 
        C = 2,
        D = 3, 
        E = 4,
        F = 5, 
        G = 6, 
        H = 7,
        I = 8
    }

}
