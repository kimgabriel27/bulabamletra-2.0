using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathScript : MonoBehaviour
{
    MainGameScript MScript;
    GameManager gameManager;
    // Start is called before the first frame update
    void Start()
    {
        MScript = MainGameScript.GetInstance;
        gameManager = GameManager.instance;
    }



    private void OnTriggerStay2D(Collider2D collision)
    {
     
        if (collision.GetComponent<Bubble>() && collision.GetComponent<Bubble>().isFixed)
        {

            gameManager.ShowLoseScreen();
          //  gameManager.blackBG.SetActive(true);
            GameEvents.current.PlayerLose();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("Bubble Touch");
        if (collision.GetComponent<Bubble>() && collision.GetComponent<Bubble>().isFixed)
        {

            MScript.ShowLoseScreen();
        //    MScript.blackBG.SetActive(true);
      //      GameEvents.current.PlayerLose();
        }
    }

}
