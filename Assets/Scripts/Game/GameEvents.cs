using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEvents : MonoBehaviour
{
    public static GameEvents current;

    private void Awake()
    {
        current = this;
    }

    public event Action OnLetterDestroyEvent;
    public event Action OnLetterScoreEvent;
    public event Action OnWordsLoaded;
    public event Action<GameObject> OnLetterSpawn;
    public event Action OnPlayerLose;
    public event Action OnWrongWord;
    public event Action OnLetterMove;
    public event Action OnLevelReset;

    public void LevelReset()
    {
        if(OnLevelReset != null)
        {
            OnLevelReset();
        }
    }
   
    public void LetterMove()
    {
        if(OnLetterMove != null)
        {
            OnLetterMove();
        }
    }

    public void LoadText()
    {
        if (OnWordsLoaded != null)
        {
            OnWordsLoaded();
        }
    }

    public void DestroyLetter()
    {
        if (OnLetterDestroyEvent != null)
        {
            OnLetterDestroyEvent();
        }
    }

    public void ScoreLetter()
    {
        if (OnLetterScoreEvent != null)
        {
            OnLetterScoreEvent();
        }
    }

    public void SpawnLetter(GameObject Letter)
    {
        if (OnLetterSpawn != null)
        {
            OnLetterSpawn(Letter);
        }
    }

    public void PlayerLose()
    {
        if(OnPlayerLose != null)
        {
            OnPlayerLose();
        }
    }

    public void WrongWord()
    {
        if(OnWrongWord != null)
        {
            OnWrongWord();
        }
    }
}
