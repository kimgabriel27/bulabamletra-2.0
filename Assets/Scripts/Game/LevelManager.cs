using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class LevelManager : MonoBehaviour
{

    #region Singleton
    public static LevelManager instance;

    //public static LevelManager GetInstance
    //{
    //    get
    //    {
    //        if (instance == null)
    //        {
    //            instance = GameObject.FindObjectOfType<LevelManager>();

    //        }
    //        return instance;
    //    }
    //}

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        //InitLevelManager();
    }
    #endregion
    [System.Serializable]
    public class BubbleLetter
    {
        public string tag;
        public GameObject LetterPrefab;
    }

    public Grid grid;
    public Transform bubblesArea;
    public List<BubbleLetter> bubblesPrefabs;
    // public List<GameObject> LetterBubblesPrefab;
    public List<GameObject> bubblesInScene;
    public List<string> lettersInScene;

    public float offset;
    public GameObject eightBubbleLine;
    public GameObject sevenBubbleLine;
    [SerializeField] private bool lastLineIsSeven = false;
    public char[] TargetWordArray;
    public Dictionary<string, GameObject> bubblesTargetWordList;
    public List<GameObject> bubblesLetterInTargetWord = new List<GameObject>();

    public List<string> tagList = new List<string>();
    public WordManager wordManager;

    [Header("Level Designs")]
    public List<GameObject> assignedLevelDesigns = new List<GameObject>();
    public List<GameObject> easyLevelDesign = new List<GameObject>();
    public List<GameObject> mediumLevelDesign = new List<GameObject>();
    public List<GameObject> hardLevelDesign = new List<GameObject>();


    //public string[] tagList;
    int count = 0;
    string difficulty;
    static int number = 0;
    float yPos = 3.73f;
    int lastValue;

    private void Start()
    {

        string word = GameManager.instance.TargetWord;
        wordManager = WordManager.Instance;
        bubblesTargetWordList = new Dictionary<string, GameObject>();
        grid = GetComponent<Grid>();

        TargetWordArray = new char[word.ToCharArray().Length];
        TargetWordArray = word.ToCharArray();

        difficulty = WordManager.Instance.difficulty;
        Debug.Log(difficulty + "difficulty");
  

        if (difficulty != null && assignedLevelDesigns != null)
        {
            Debug.Log("Adding Level");
            if (difficulty == "Easy")
            {
                // this.transform.position = new Vector3(this.transform.position.x, yPos, 0);
                foreach (GameObject p in easyLevelDesign)
                {
                    assignedLevelDesigns.Add(p);

                }
            }
            else if (difficulty == "Average")
            {
              // this.transform.position = new Vector3(this.transform.position.x, yPos, 0);
                foreach (GameObject p in mediumLevelDesign)
                {
                    assignedLevelDesigns.Add(p);

                }
            }
            else if (difficulty == "Hard")
            {
                //.transform.position = new Vector3(this.transform.position.x, yPos, 0);
                foreach (GameObject p in hardLevelDesign)
                {
                    assignedLevelDesigns.Add(p);

                }
            }

        }


        for (int i = 0; i < TargetWordArray.Length; i++)
        {
            foreach (BubbleLetter bubbleLetter in bubblesPrefabs)
            {
                if (TargetWordArray[i].ToString() == bubbleLetter.tag)
                {
                    bubblesLetterInTargetWord.Add(bubbleLetter.LetterPrefab);
                }
            }
        }

        //foreach (GameObject p in assignedLevelDesigns)
        //{
        //    if (assignedLevelDesigns[0] == p)
        //    {
        //        p.SetActive(true);
        //    }
        //    else
        //    {
        //        p.SetActive(false);
        //    }

        //}

        transform.position = assignedLevelDesigns[number].transform.position;

        if (assignedLevelDesigns[number].transform.position.y == 2.09f)
        {
            lastLineIsSeven = true;
        }
        Debug.Log(assignedLevelDesigns[number].name);
        GenerateLevel(assignedLevelDesigns[number]);
        number++;


    }

    public void GenerateLevel(GameObject levelDesign)
    {
        Debug.Log(bubblesLetterInTargetWord.Count);

        FillWithBubbles(levelDesign, bubblesLetterInTargetWord);
        SnapChildrensToGrid(bubblesArea);
        UpdateListOfBubblesInScene();

        // assignedLevelDesigns.Remove(assignedLevelDesigns[0]);
    }

    #region Snap to Grid
    private void SnapChildrensToGrid(Transform parent)
    {
        foreach (Transform t in parent)
        {
            SnapToNearestGripPosition(t);
        }
    }

    public void SnapToNearestGripPosition(Transform t)
    {
        Vector3Int cellPosition = grid.WorldToCell(t.position);
        t.position = grid.GetCellCenterWorld(cellPosition);
    }
    #endregion

    #region Add new line
    [ContextMenu("AddLine")]
    public void AddNewLine()
    {
        OffsetGrid();
        OffsetBubblesInScene();


        GameObject newLine = lastLineIsSeven == true ? Instantiate(sevenBubbleLine) : Instantiate(eightBubbleLine);
        FillNewLineBubbles(newLine, bubblesLetterInTargetWord);

        // FillWithBubbles(newLine, bubblesLetterInTargetWord);

        SnapChildrensToGrid(bubblesArea);
        lastLineIsSeven = !lastLineIsSeven;

    }

    private void OffsetGrid()
    {
        transform.position = new Vector2(transform.position.x, transform.position.y - offset);
    }

    private void OffsetBubblesInScene()
    {
        foreach (Transform t in bubblesArea)
        {
            t.transform.position = new Vector2(t.position.x, t.position.y - offset);
        }
    }
    #endregion

    private void FillWithBubbles(GameObject go, List<GameObject> bubbles)
    {
        Debug.Log(bubbles.Count);
        foreach (Transform t in go.transform)
        {
            //string randLetter = TargetWordArray[Random.Range(0, TargetWordArray.GetUpperBound(0))].ToString();
            //var bubble = Instantiate(bubbles[(int)(Random.Range(0, bubbles.Count * 1000000f) / 1000000f)], bubblesArea);

            int randNum = count % TargetWordArray.Length;
            //var bubble = Instantiate(bubbles[(int)(randNum)], bubblesArea);
            var bubble = Instantiate(bubbles[(int)(t.gameObject.GetComponent<LetterSpawnScript>().numberLetter)], bubblesArea);

            bubble.transform.position = t.position;
            count++;
        }

        // Destroy(go);
    }

    public void UpdateListOfBubblesInScene()
    {
        Debug.Log("Update List of Bubbles");
        List<string> letters = new List<string>();
        List<GameObject> newListOfBubbles = new List<GameObject>();

        foreach (Transform t in bubblesArea)
        {
            Bubble currBubble = t.gameObject.GetComponent<Bubble>();
            // Bubble bubbleScript = t.GetComponent<Bubble>();
            if (letters.Count < bubblesPrefabs.Count && !letters.Contains(currBubble.bubbleLetter.ToString()))
            {

                string letter = currBubble.bubbleLetter.ToString();
                letters.Add(letter);

                for (int i = 0; i < bubblesPrefabs.Count; i++)
                {
                    if (letter.Equals(bubblesPrefabs[i].tag))
                    {
                        Debug.Log("new list bubble added");
                        newListOfBubbles.Add(bubblesPrefabs[i].LetterPrefab);
                    }
                }

                //Adding Rigidbody2d
                if (!t.gameObject.GetComponent<Rigidbody2D>() && !t.gameObject.GetComponent<Bubble>().isConnected)
                {
                    Rigidbody2D rb = t.gameObject.AddComponent<Rigidbody2D>();
                    rb.gravityScale = 1;
                    rb.constraints = RigidbodyConstraints2D.FreezeRotation;

                }
                //foreach (GameObject prefab in bubblesPrefabs)
                //{

                //    if (/*prefab.tag.ToString() == letter || */letter.Equals(prefab.GetComponent<Bubble>().bubbleLetter.ToString()))
                //    {

                //       // Debug.Log(t.gameObject.name);
                //        newListOfBubbles.Add(prefab);
                //    }
                //}
            }
        }

        lettersInScene = letters;
        bubblesInScene = newListOfBubbles;





    }

    public void SetAsBubbleAreaChild(Transform bubble)
    {
        SnapToNearestGripPosition(bubble);
        bubble.SetParent(bubblesArea);
    }

    void InitLevelManager()
    {
        if (instance == null)
        {
            DontDestroyOnLoad(gameObject);
            instance = this;
        }
        else
        {
            if (instance != this)
            {
                Destroy(gameObject);
            }
        }
    }

    void FillNewLineBubbles(GameObject go, List<GameObject> bubbles)
    {
        Debug.Log(bubbles.Count);
        foreach (Transform t in go.transform)
        {
            //string randLetter = TargetWordArray[Random.Range(0, TargetWordArray.GetUpperBound(0))].ToString();
            //var bubble = Instantiate(bubbles[(int)(Random.Range(0, bubbles.Count * 1000000f) / 1000000f)], bubblesArea);

            int randNum = count % TargetWordArray.Length;
            var bubble = Instantiate(bubbles[(int)(randNum)], bubblesArea);

            //  var bubble = Instantiate(bubbles[(int)(t.gameObject.GetComponent<LetterSpawnScript>().numberLetter)], bubblesArea);

            bubble.transform.position = t.position;
            count++;
        }

    }

    public int UniqueRandomInt()
    {
        int val = Random.Range(0, TargetWordArray.Length);
        while (lastValue == val)
        {
            val = Random.Range(0, TargetWordArray.Length);
        }
        lastValue = val;
        return val;
    }
    public void SetNumber()
    {
        number = 0;
    }

    public void ListClear()
    {
        assignedLevelDesigns.Clear();
       
    }
}
