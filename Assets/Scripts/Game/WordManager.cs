using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WordManager : MonoBehaviour
{
    public static WordManager Instance;

    [SerializeField]
    TextAsset wordList;

    //public List<WordStruct> Easy = new List<WordStruct>();
    //public List<WordStruct> Average = new List<WordStruct>();
    //public List<WordStruct> Hard = new List<WordStruct>();

    //public List<string> EasyWords;
    //public List<string> AveWords;
    //public List<string> HardWords;
    [SerializeField]
    public static List<GameObject> targetWordList = new List<GameObject>();
    
    public List<GameObject> Words;
    public List<GameObject> EWords;
    public List<GameObject> MWords;
    public List<GameObject> HWords;

    public string targetWord;
    public GameObject wordPrefab;

    public int WordNumber;

    //0 = easy
    //1 = ave
    //2 = hard

    public int GameDifficulty = 1;

    public string difficulty;
    
    void Awake()
    {
        InitWordManager();
        //GameEvents.current.OnLevelReset += ChooseAnotherWord;
        //CreateWords();
    }


    public void SetDifficulty(int DifNumber)
    {
        GameDifficulty = DifNumber;
        GetRandomWords();
    }

    void InitWordManager()
    {
        if (Instance == null)
        {
            DontDestroyOnLoad(gameObject);
            Instance = this;
        }
        else
        {
            if (Instance != this)
            {
                Debug.Log("Instance of Word Manager Destroy");
                Destroy(gameObject);
            }
        }
    }

    public int GetWordListCount()
    {
        return targetWordList.Count;
    }

    public void GetRandomWords() // words per difficulty
    {
      //  int N = GetComponent<WordManager>().Words.Count;
        int RandomWordNumber = 0;
        targetWordList.Clear();
        Debug.Log("Getting Words");
        switch (GameDifficulty)
        {
            case 0:

                difficulty = "Easy";
                Debug.Log("easy");
              
                RandomWordNumber = Random.Range(0, EWords.Count);

                targetWord = EWords[0].name;
                wordPrefab = EWords[0];
            
                foreach (var i in EWords)
                {
                  
                    targetWordList.Add(i); 
                }
               
                targetWordList.Remove(EWords[0]);
     
                break;

            case 1:

                Debug.Log("ave");
                difficulty = "Average";
                RandomWordNumber = Random.Range(0, MWords.Count);

                targetWord = MWords[0].name;
                wordPrefab = MWords[0];

                foreach (var i in MWords)
                {
                    targetWordList.Add(i);
                }

                targetWordList.Remove(MWords[0]);
                
                break;

            case 2:
                Debug.Log("hard");
                difficulty = "Hard";
                RandomWordNumber = Random.Range(0, HWords.Count);
                targetWord = HWords[0].name;
                wordPrefab = HWords[0];
         
                foreach (var i in HWords)
                {
              
                    targetWordList.Add(i);

                }

                targetWordList.Remove(HWords[0]);
                
                break;

            default:
                break;
        }



        // int RandomWordNumber = Random.Range(0, N - 1);

        // RandomWord = WManager.GetComponent<WordManager>().Words[RandomWordNumber];
        //TargetWord = WManager.GetComponent<WordManager>().Words[RandomWordNumber].name;
    }

    public void ChooseAnotherWord()
    {
       
        int randNum = Random.Range(0, targetWordList.Count);
        targetWord = targetWordList[0].name;
        wordPrefab = targetWordList[0];
        targetWordList.Remove(targetWordList[0]);
    }
    
    public int GetWordDifficulty()
    {
        return GameDifficulty;
    }
   
}

[System.Serializable]
public struct WordStruct
{
    public string Word;
    public AudioClip clip;

    public WordStruct(string mWord, AudioClip mClip)
    {
        Word = mWord;
        clip = mClip;
    }

}