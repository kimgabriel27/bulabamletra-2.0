using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LetterManager : MonoBehaviour
{
    public static LetterManager Instance;

    public List<GameObject> LetterList;

    // Start is called before the first frame update
    void Awake()
    {
        InitLetterManager();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void InitLetterManager()
    {
        if (Instance == null)
        {
            DontDestroyOnLoad(gameObject);
            Instance = this;
        }
        else
        {
            if (Instance != this)
            {
                Destroy(gameObject);
            }
        }
    }
}
