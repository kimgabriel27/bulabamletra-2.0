using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public static PauseMenu Instance;

    public bool GamePaused = false;
    public GameObject PauseMenuUI;
    public GameObject blackBG;
    LevelManager levelManager;

    public void Awake()
    {
        Instance = this;
        
    }

    private void Start()
    {
        levelManager = LevelManager.instance;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (GamePaused)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }
    }

    public void Resume()
    {
       
        blackBG.SetActive(false);
        PauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        GamePaused = false;
        GameManager.instance.ShooterDisable(true);
    }

    void Pause()
    {
        GameManager.instance.ShooterDisable(false);
        blackBG.SetActive(true);
        PauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        GamePaused = true;
    }

    public void LoadMainMenu()
    {
        Time.timeScale = 1f;
        levelManager.SetNumber();
        levelManager.ListClear();
        SceneManager.LoadScene("MainMenu");
        //SceneManager.LoadScene
    }

    public void QuitGame()
    {
        GamePaused = false;
        Time.timeScale = 1f;
    }

    public void PauseGame(GameObject SettingScreen)
    {
        GamePaused = !GamePaused;
    }
}
