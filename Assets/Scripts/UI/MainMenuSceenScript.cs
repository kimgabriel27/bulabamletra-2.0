using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class MainMenuSceenScript : MonoBehaviour
{
    public GameObject CreditsScreen;
    public GameObject CreditsButton;
    public GameObject PlayButton;

    public GameObject loadingScreen;
    public Slider slider;
    public Text progressText;
    WordManager WManager;

    private void Start()
    {
        WManager = WordManager.Instance;
    }

    private void Update()
    {
    }

    public void LoadCredits()
    {
        CreditsScreen.SetActive(true);
    }

    public void SelectDifficulty(GameObject Panel)
    {
        Panel.SetActive(!Panel.activeInHierarchy);
    }

    public void LoadMainGame(int DifficultyNumber)
    {
        WordManager.Instance.SetDifficulty(DifficultyNumber);

        StartCoroutine(LoadAsynchronously("MainGame"));
       
     //   SceneManager.LoadScene("MainGame", LoadSceneMode.Single);
    }

    IEnumerator LoadAsynchronously (string sceneName)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneName);

        loadingScreen.SetActive(true);
        while (!operation.isDone)
        {
            float progress = Mathf.Clamp01(operation.progress / .9f);

            slider.value = progress;
            progressText.text = (progress * 100f) + "%"; 
           Debug.Log(progress);

            yield return null;
        }

        yield return new WaitForSeconds(2);
    }
}
