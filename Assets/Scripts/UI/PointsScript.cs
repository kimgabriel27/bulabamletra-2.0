using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PointsScript : MonoBehaviour
{
    public Text LetterScore;

    // Start is called before the first frame update
    private void Start()
    {
        LetterScore = gameObject.GetComponent<Text>();

        LetterScore.text = "Points: " + MainGameScript.CurrentScore.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        LetterScore.text = "Points: " + MainGameScript.CurrentScore.ToString();
    }
}
