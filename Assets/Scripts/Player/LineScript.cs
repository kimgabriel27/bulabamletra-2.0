using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class LineScript : MonoBehaviour
{
    Vector3 startPos;
    Vector3 endPos;
    Vector3 mousePos;
    Vector3 mouseDir;
    Camera camera;
    private LineRenderer lr;
    public float max = 5;

    Vector2 direction;
    int rayDistance = 50;
    List<Vector3> Points;
    int currentReflections = 0;
    int maxReflections = 1;

    const int Infinity = 999;


    // Start is called before the first frame update
    void Start()
    {
        Points = new List<Vector3>();
        lr = GetComponent<LineRenderer>();
        lr.useWorldSpace = true;
        camera = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
       
        mousePos = camera.ScreenToWorldPoint(Input.mousePosition);
        mousePos.z = 0;
        mouseDir = mousePos - gameObject.transform.position;
        mouseDir.z = 0;
        mouseDir = mouseDir.normalized;

        if(/*Input.GetMouseButtonDown(0) &&*/ mousePos.x < 0 && mousePos.y > -5.0f && !GameManager.instance.isTutorial && !PauseMenu.Instance.GamePaused)
        {
            lr.enabled = true;
            startPos = gameObject.transform.position;
            startPos.z = 0;
            lr.SetPosition(0, startPos);
            endPos = mousePos;
            endPos.z = 0;

            RaycastHit2D hit = Physics2D.Raycast(transform.position, (mousePos - startPos).normalized, rayDistance);
            Debug.DrawLine(transform.position, hit.point);
            currentReflections = 0;
            Points.Clear();
            Points.Add(transform.position);

            if (hit)
            {
                ReflectFurther(transform.position, hit);

            }
            else
            {
                Points.Add(transform.position + (mousePos - startPos).normalized * Infinity);
            }

            try
            {
                if (hit.transform.gameObject.GetComponent<Bubble>())
                {
                    maxReflections = 0;
                }

                else
                {
                    maxReflections = 1;
                }
            }   

            catch (NullReferenceException err)
            {
                maxReflections = 1;

            }

            lr.positionCount = Points.Count;
            lr.SetPositions(Points.ToArray());
        }
        //if(Input.GetMouseButton(0) && mousePos.x < 0)
        //{
        //    startPos = gameObject.transform.position;
        //    startPos.z = 0;
        //    lr.SetPosition(0, startPos);
        //    endPos = mousePos;
        //    endPos.z = 0;

        //    //float capLength = Mathf.Clamp(Vector2.Distance(startPos, endPos), 0, max);
        //    //endPos = startPos + (mouseDir * capLength);
        //    //lr.SetPosition(1, endPos);


        //    RaycastHit2D hit = Physics2D.Raycast(transform.position, (mousePos - startPos).normalized, rayDistance);
   
        //    currentReflections = 0;
        //    Points.Clear();
        //    Points.Add(transform.position);

        //    if (hit)
        //    {
        //        ReflectFurther(transform.position, hit);

        //    }
        //    else
        //    {
        //        Points.Add(transform.position + (mousePos - startPos).normalized * Infinity);
        //    }

        //    try
        //    {
        //        if (hit.transform.gameObject.GetComponent<Bubble>())
        //        {
        //            maxReflections = 0;
        //        }

        //        else
        //        {
        //            maxReflections = 1;

        //        }
        //    }

        //    catch (NullReferenceException err)
        //    {
        //        maxReflections = 1;

        //    }
       

        //    lr.positionCount = Points.Count;
        //    lr.SetPositions(Points.ToArray());
        //}
        if(Input.GetMouseButtonUp(0) && mousePos.x < 0)
        {
            //lr.enabled = false;
        }

    }

    void ReflectFurther(Vector2 origin, RaycastHit2D hitData)
    {
        if (currentReflections > maxReflections) return;

        Points.Add(hitData.point);
        currentReflections++;

        Vector2 inDirection = (hitData.point - origin).normalized;
        Vector2 newDirection = Vector2.Reflect(inDirection, hitData.normal);

        var newHitData = Physics2D.Raycast(hitData.point + (newDirection * 0.0001f), newDirection * 100, rayDistance);
        if (newHitData)
        {
            ReflectFurther(hitData.point, newHitData);
        }
        else
        {
            Points.Add(hitData.point + newDirection * rayDistance);
        }
    }

    public void LineActive( bool isActive)
    {
        lr.enabled = isActive;
    }


}
