using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Launcher : MonoBehaviour
{
    public GameObject ball;
    public GameObject load;
    [System.Serializable]
    public class Letters
    {
        public string tag;  
        public GameObject LetterPrefab;
    }

    public char[] TargetWordArray;
    public string[] letterArr;
    [SerializeField] private List<Letters> LetterList;
    public Dictionary<string, GameObject> LetterCollection = new Dictionary<string, GameObject>();
    public List<int> TakeList = new List<int>();
    public string word;
    int randNum;
    int count;
   

    [SerializeField] public MainGameScript mainGameScript;

    public Transform bulletSpawn;
    public const float LAUNCH_SPEED = 15f;

    void Start()
    {
        mainGameScript = GameObject.FindGameObjectWithTag("MainGame").GetComponent<MainGameScript>();
       // word = TargetWordArray.ToString();
        LetterCollection = new Dictionary<string, GameObject>();
      

        //Debug.Log(word);

        //for (int i = 0; i < word.Length; i++)
        //{
        //  //  Debug.Log(word);
        //    letterArr[i] = mainGameScript.TargetWord[i].ToString();
        //}
        
        foreach (Letters letters in LetterList)
        {
            LetterCollection.Add(letters.tag, letters.LetterPrefab);
        }

       count++;

        if (TakeList.Count == 0)
        {
            TakeList.Add(count);
        }

        //randNum = Random.Range(0, TakeList.Count);
        // Debug.Log(randNum);

        //newLetter = TargetWordArray[0]; //Need tag
        //  Create(position, newLetter.ToString());
        //Load(newLetter.ToString());

        // TakeList.RemoveAt(randNum);

    }

    void Update()
    {
 
        Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector2 delta = mousePos - new Vector2(transform.position.x, transform.position.y);
        transform.rotation = Quaternion.Euler(0f, 0f, 90 - Mathf.Rad2Deg * Mathf.Atan2(delta.x, delta.y));

        if (Input.GetMouseButtonDown(0))
        {
            Fire();
        }
    }

    public void Load(string tag)
    {
        if (load == null)
        {
            load = (GameObject)Instantiate(LetterCollection[tag], transform.position, transform.rotation);
            load.SetActive(true);

            CircleCollider2D collider = load.GetComponent<CircleCollider2D>();
            if (collider != null)
                collider.enabled = false;

            Hitter hitter = load.GetComponent<Hitter>();
            if (hitter != null)
                hitter.parent = gameObject;
        }
    }


    void Fire()
    {
        if (load != null)
        {

            CircleCollider2D collider = load.GetComponent<CircleCollider2D>();
            if (collider != null)
                collider.enabled = true;

            Rigidbody2D rb = load.GetComponent<Rigidbody2D>();
            if (rb != null)
            {
                rb.velocity = transform.right * LAUNCH_SPEED;
            }
        }
    }
}
