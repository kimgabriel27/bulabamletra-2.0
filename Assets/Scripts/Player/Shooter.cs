using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Shooter : MonoBehaviour
{
    [System.Serializable]
    public class BubbleLetter
    {
        public string tag;
        public GameObject LetterPrefab;
    }

    public Transform gunSprite;
    public bool canShoot;
    public float speed = 6f;

    public Transform nextBubblePosition;
    public GameObject currentBubble;
    public GameObject nextBubble;

    private Vector2 lookDirection;
    private float lookAngle;
    public bool isSwaping = false;
    public float time = 0.02f;
    int lastValue = 1;

    [Header("Collections")]
    public List<BubbleLetter> bubbleLetter;
    public List<GameObject> bubblesInScene;
    public List<string> letters;
    public Dictionary<string, GameObject> bubbleDictionary = new Dictionary<string, GameObject>();
    public List<string> targetLetters = new List<string>();
    LevelManager levelManager;
    GameManager gameManager;
    char[] targetChar;

    float[] chances;

    int numberOFBubble = 0;
    int nextNumber = 0;
    private void Start()
    {
        targetChar = new char[GameManager.instance.TargetWord.ToCharArray().Length];
        levelManager = GameObject.FindGameObjectWithTag("LevelManager").GetComponent<LevelManager>();
        gameManager = GameManager.instance;
        targetChar = new char[WordManager.Instance.targetWord.ToCharArray().Length];
        targetChar = WordManager.Instance.targetWord.ToCharArray();

   
        foreach(BubbleLetter a in  bubbleLetter)
        {
            bubbleDictionary.Add(a.tag, a.LetterPrefab);
           
        }
        
        foreach(char a in targetChar )
        {
            targetLetters.Add(a.ToString());
        }
    }

    public void Update()
    {
       
        lookDirection = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
        lookAngle = Mathf.Atan2(lookDirection.y, lookDirection.x) * Mathf.Rad2Deg;
        gunSprite.rotation = Quaternion.Euler(0f, 0f, lookAngle - 90f);

        Vector3 mouseScreen = Input.mousePosition;
        Vector3 mouse = Camera.main.ScreenToWorldPoint(mouseScreen);

     //   Debug.Log("Mouse x:" + mouse.x + " | Mouse y:" + mouse.y);
        if (isSwaping)
        {
            //if (Vector2.Distance(currentBubble.transform.position, nextBubblePosition.position) <= 0.2f
            //    && Vector2.Distance(nextBubble.transform.position, transform.position) <= 0.2f)
            //{
            //    nextBubble.transform.position = transform.position;
            //    currentBubble.transform.position = nextBubblePosition.position;

            //    GameObject reference = currentBubble;
            //    currentBubble = nextBubble;
            //    nextBubble = reference;

            Vector3 temp = currentBubble.transform.position;
            nextBubble.transform.position = Vector2.Lerp(nextBubble.transform.position, temp, time);
            currentBubble.transform.position = Vector2.Lerp(currentBubble.transform.position, nextBubblePosition.position, time);

            currentBubble.GetComponent<Collider2D>().enabled = true;
            nextBubble.GetComponent<Collider2D>().enabled = true;

            GameObject reference = currentBubble;
            currentBubble = nextBubble;
            nextBubble = reference;
            isSwaping = false;
        }
    }

    public void Shoot()
    {
        transform.rotation = Quaternion.Euler(0f, 0f, lookAngle - 90f);
        currentBubble.transform.rotation = transform.rotation;
        currentBubble.GetComponent<Rigidbody2D>().AddForce(currentBubble.transform.up * speed, ForceMode2D.Impulse);
        currentBubble = null;
    }

    [ContextMenu("SwapBubbles")]
    public void SwapBubbles()
    {
        currentBubble.GetComponent<Collider2D>().enabled = false;
        nextBubble.GetComponent<Collider2D>().enabled = false;
        isSwaping = true;
    }

    [ContextMenu("CreateNextBubble")]
    public void CreateNextBubble()
    {
        /* List<GameObject>*/
        bubblesInScene = levelManager.bubblesInScene;
        /* List<string>*/
        letters = levelManager.lettersInScene;

        if (nextBubble == null)
        {
            nextBubble = InstantiateNewBubble(bubblesInScene, bubbleDictionary);
        }
        else
        {
            if (!letters.Contains(nextBubble.GetComponent<Bubble>().bubbleLetter.ToString()))
            {
              //  Debug.Log(letters.Contains(nextBubble.GetComponent<Bubble>().name.ToString()));
                Destroy(nextBubble);
                nextBubble = InstantiateNewBubble(bubblesInScene, bubbleDictionary);
            }
        }

        if (currentBubble == null)
        {
            currentBubble = nextBubble;
            currentBubble.transform.position = new Vector2(gunSprite.position.x, gunSprite.position.y);
            nextBubble = InstantiateNewBubble(bubblesInScene, bubbleDictionary);
        }
    }

    private GameObject InstantiateNewBubble(List<GameObject> bubblesInScene, Dictionary<string, GameObject> bubbleDict)
    {
        int randNum = UniqueRandomInt();
        
        //Instantiate(bubbleDict[targetLetters[randNum]]);
     //   Debug.Log( " Capacity - " + targetLetters.Count);

        GameObject newBubble = Instantiate(bubbleDict[targetLetters[randNum]]);
        newBubble.transform.position = new Vector2(nextBubblePosition.position.x, nextBubblePosition.position.y);
        newBubble.GetComponent<Bubble>().isFixed = false;
        Rigidbody2D rb2d = newBubble.AddComponent(typeof(Rigidbody2D)) as Rigidbody2D;
        rb2d.gravityScale = 0f;

        return newBubble;
    }

    public int UniqueRandomInt()
    {
        if(numberOFBubble < 1 )
        {
            numberOFBubble++;
            return nextNumber;
        }
        else
        {
            numberOFBubble = 0;
            nextNumber++;
        }

        if(nextNumber >= targetLetters.Count)
        {
            nextNumber = 0;
        }
        return nextNumber;
        //int val = Random.Range(0, targetLetters.Count);
        //while(lastValue == val)
        //{
        //    val = Random.Range(0, targetLetters.Count);
        //}
        //lastValue = val;
        //return val;
    }

    public void RandomGeneratedBubble()
    {
        List<string> lettersInWord = new List<string>();

        if(GameManager.instance.CurrentWord != null || GameManager.instance.CurrentWord.Length == 0)
        {
            foreach (char a in GameManager.instance.CurrentWord)
            {
                lettersInWord.Add(a.ToString());
            }
        }
        else
        {
            int randNum = Random.Range(0, 100);
            for (int i = 0; i < chances.Length; i++)
            {
                
            }
        
        }
      
        
       
    }
}
