using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour
{
    [System.Serializable]
    public class Letters
    {
        public string tag;
        public GameObject LetterPrefab;
    }

    ObjectPoolScript LetterPool;
    WordManager WManager;
    PauseMenu PauseScript;
    MainGameScript mainGame;
    LineRenderer lr;
    Vector2 StartLineVector;
    Vector2 EndLineVector;

    //Temporary fix for lose condition
    [SerializeField] private GameObject border;
    private Coroutine borderCoroutine;

    public Transform BulletSpawnPoint;
    public Transform BulletPreview;
    public Transform BulletPreview2;   
    public Transform BulletPreview3;
    public GameObject LetterToShoot;
    public GameObject Prev2;
    public GameObject Prev3;

    public GameObject load;

    public char[] PlayerLetterArray;
    public List<string> PlayerLetterList;
    public List<int> PlayerRandomNumbers;


    [SerializeField] private List<Letters> LetterList;
    public Dictionary<string, GameObject> LetterCollection = new Dictionary<string, GameObject>();

    public bool canShoot = true;
    public bool isSwaping = false;
    public float time = 0.02f;
    public const float LAUNCH_SPEED = 15f;
    GameObject MGobject;

    int randNum = 0;

    // Start is called before the first frame update
    void Start()
    {
        MGobject = GameObject.FindGameObjectWithTag("MainGame");
        LetterPool = ObjectPoolScript.Instance;
        WManager = WordManager.Instance;
        PauseScript = PauseMenu.Instance;
        mainGame = GameObject.FindGameObjectWithTag("MainGame").GetComponent<MainGameScript>();

        lr = GetComponent<LineRenderer>();

        LetterCollection = new Dictionary<string, GameObject>();
        foreach (Letters letters in LetterList)
        {
            LetterCollection.Add(letters.tag, letters.LetterPrefab);
        }
     
        //Debug.Log(PlayerLetterArray[0]);
        // lr.positionCount = 2;
        //  lr.SetVertexCount(2);
   
    }

    // Update is called once per frame
    void Update()
    {
        if (PauseScript.GamePaused || MainGameScript.GetInstance.LoseScreen.activeSelf)
        {
            Debug.Log("Paused");
            return;
        }
    
        Vector3 mouseScreen = Input.mousePosition;
        Vector3 mouse = Camera.main.ScreenToWorldPoint(mouseScreen);

        transform.rotation = Quaternion.Euler(0, 0, Mathf.Atan2(mouse.y - transform.position.y, mouse.x - transform.position.x) * Mathf.Rad2Deg - 90);

        StartLineVector = transform.position;
        EndLineVector = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        if (Input.GetMouseButtonUp(0) && mouse.x < 0 && mouse.y > -3 && canShoot && !isSwaping)
        {
            //MovePrev3To2();
            //MovePrev2ToShoot();
           
           // Load();
            Fire();

            //MGobject.GetComponent<GridManager>().AdjustGridMembers(0);
            //ShootLetter();

            //// Temporary fix for lose condition
            //if (borderCoroutine != null)
            //    StopCoroutine(borderCoroutine);
            //borderCoroutine = StartCoroutine(DisableBorder());
        }


        if (Input.GetMouseButtonUp(1) && mouse.x < 0 && mouse.y > -3 && !isSwaping)
        {
                isSwaping = true;
            
                Prev2.transform.position = BulletPreview.position;
                LetterToShoot.transform.position = BulletPreview2.position;


                LetterToShoot.GetComponent<Collider2D>().enabled = true;
                Prev2.GetComponent<Collider2D>().enabled = true;
                isSwaping = false;

                GameObject reference = LetterToShoot;
                LetterToShoot = Prev2;
                Prev2 = reference;
                
            

            Prev2.transform.position = Vector2.Lerp(Prev2.transform.position, BulletPreview.position, time);
            LetterToShoot.transform.position = Vector2.Lerp(LetterToShoot.transform.position, BulletPreview2.position, time);
        }
        //  lr.SetPosition(0, StartLineVector);
        //   lr.SetPosition(1, EndLineVector);
    }

    // Temporary fix for lose condition
    private IEnumerator DisableBorder()
    {
        float duration = 0.75f;
        float currTime = 0;

        // border.SetActive(false);

        while (currTime < duration)
        {
            currTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        border.SetActive(true);

    }

    public void MovePrev2ToShoot()
    {
        if (Prev2)
        {
            Prev2.transform.position = BulletPreview.transform.position;
            LetterToShoot = Prev2;
           
        }
    }

    public void MovePrev3To2()
    {
        if (Prev3)
        {
            Prev3.transform.position = BulletPreview2.transform.position;
            Prev2 = Prev3;
        }
    }

    public void GetRandomBullet()
    {
        int SpawnerRandomNumber;
        SpawnerRandomNumber = Random.Range(0, 100);
        char c = mainGame.LetterArray[Random.Range(0, mainGame.LetterArray.Length - 1)];

        if (SpawnerRandomNumber >= 15)
        {

            //Prev3 = LetterPool.SpawnFromPool(PlayerLetterArray[Random.Range(0, PlayerLetterArray.Length)].ToString(), BulletPreview3.transform.position, Quaternion.identity);
            //Prev3 = LetterPool.SpawnFromPool(c.ToString(), BulletPreview3.transform.position, Quaternion.identity);
            //Prev3 = (GameObject)Instantiate(LetterCollection[c.ToString()], BulletPreview3.transform.position, Quaternion.identity);
            Prev3.GetComponent<Hitter>().parent = GameObject.FindGameObjectWithTag("MainGame");

        }
        else
        {
            Prev3 = LetterPool.SpawnFromPool(PlayerLetterList[PlayerRandomNumbers[Random.Range(0, PlayerRandomNumbers.Count)]], BulletPreview3.transform.position, Quaternion.identity);
        }

        Prev3.name = "Preview";

        
    }

    public void SwapBubbles()
    {
        LetterToShoot.GetComponent<Collider2D>().enabled = false;
        Prev2.GetComponent<Collider2D>().enabled = false;
        isSwaping = true;
    }

    void ShootLetter()
    {
        if (LetterToShoot.transform.position == BulletPreview.position)
        {
            LetterToShoot.GetComponent<Collider2D>().enabled = true;
            Debug.Log("Shoot Bullet");
            LetterToShoot.name = "Bullet";
            //LetterToShoot.gameObject.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
            LetterToShoot.transform.position = BulletSpawnPoint.transform.position;
            LetterToShoot.transform.rotation = BulletSpawnPoint.transform.rotation;

            Rigidbody2D rb = LetterToShoot.GetComponent<Rigidbody2D>();
            rb.velocity = transform.up * 5;
            rb.isKinematic = false;

            canShoot = false;
            LetterToShoot = null;
        }
    }

    public void Load()
    {
        if(Prev2 == null)
        {
            Prev2 = InstantiateBubble();
        }
   

        if(LetterToShoot == null)
        {
            LetterToShoot = Prev2;
            LetterToShoot.transform.position = new Vector2(BulletPreview.position.x, BulletPreview.position.y);
            Prev2 = InstantiateBubble();
        }

    }

    void Fire()
    {
        if (LetterToShoot != null)
        {

            CircleCollider2D collider = LetterToShoot.GetComponent<CircleCollider2D>();
            if (collider != null)
                collider.enabled = true;

            Rigidbody2D rb = LetterToShoot.GetComponent<Rigidbody2D>();
            if (rb != null)
            {
                rb.velocity = transform.up * LAUNCH_SPEED;
            }

            LetterToShoot = null;
        }
    }

    private GameObject InstantiateBubble()
    {
        
        if(randNum >= mainGame.LetterArray.Length)
        {
            randNum = Random.Range(0, mainGame.LetterArray.Length);
        }
        char c = mainGame.LetterArray[randNum];
        GameObject newBubble = Instantiate(LetterCollection[c.ToString()], BulletPreview2.position, Quaternion.identity);
        newBubble.transform.position = new Vector2(BulletPreview2.position.x, BulletPreview2.position.y);
    
        // load.SetActive(true);

        CircleCollider2D collider = newBubble.GetComponent<CircleCollider2D>();
        if (collider != null)
            collider.enabled = false;

        Hitter hitter = newBubble.GetComponent<Hitter>();
        if (hitter != null)
            hitter.parent = GameObject.FindGameObjectWithTag("MainGame");
        randNum++;
        return newBubble;
    }
}
