using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

public class LetterScript : MonoBehaviour, InterfacePooledObject
{
    Rigidbody2D RB2D;
    MainGameScript MainScript;
    WordManager WManager;
    PlayerScript player;
    string LetterText;

    Animator LetterAnimation;

    public GameObject childAnim;
    public int LetterSpeed = 1;
    public float LetterSpeedModifier;
    public bool WasInteracted;
    public bool WasShot = false;

    public List<GameObject> MatchingLetters;

    Vector3 lastVelocity;

    // Start is called before the first frame update
    public void OnObjectSpawn()
    {
        gameObject.name = "Letter";

        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerScript>();
        MainScript = MainGameScript.GetInstance;
        WManager = WordManager.Instance;
        RB2D = GetComponent<Rigidbody2D>();
        LetterText = GetComponent<TextMesh>().text;
        LetterAnimation = childAnim.GetComponent<Animator>();

        MatchingLetters.Clear();

       // GameEvents.current.OnPlayerLose += DisableLetterMovement;
        GameEvents.current.OnWrongWord += MoveTheBubble; 
    }

    //public void Start()
    //{
    //    switch (WManager.GameDifficulty)
    //    {
    //        case 0:
    //            LetterSpeedModifier = 0.15f;
    //            break;

    //        case 1:
    //            LetterSpeedModifier = 0.25f;
    //            break;

    //        case 2:
    //            LetterSpeedModifier = 0.35f;
    //            break;
    //        default:
    //            break;
    //    }
    //}

    // Update is called once per frame
    void Update()
    {
        PreviewLetter();
       
        BubbleLetter();
        FixList();

        //if (MatchingLetters.Count > 0)
        //{
        //    for (int i = 0; i < MatchingLetters.Count; i++)
        //    {
        //        if (!MatchingLetters[i].activeInHierarchy)
        //        {
        //            PopThis();
                   
        //        }
        //    }
        //}
       // lastVelocity = RB2D.velocity; 
    }
    private void FixedUpdate()
    {
        BulletLetter();
    }

    private void FixList()
    {
        MatchingLetters.Distinct().ToList();
    }

    private void PreviewLetter()
    {
        if (gameObject.name == "Preview")
        {
            gameObject.GetComponent<CircleCollider2D>().enabled = false;
        }
        else
        {
            gameObject.GetComponent<CircleCollider2D>().enabled = true;
        }
    }

    private void BulletLetter()
    {
        if (gameObject.name == "Bullet")
        {
            //RB2D.AddForce(transform.up * 10, ForceMode2D.Impulse);

            //transform.Translate(Vector2.up * LetterSpeed * Time.deltaTime); 
        
            RB2D.gravityScale = 0;
            WasShot = true;
        }
    }

    private void BubbleLetter()
    {
        if (gameObject.name == "Letter")
        {
            //RB2D.velocity = new Vector2(0f, 0f);
            //transform.Translate(Vector2.up * (-LetterSpeed * (LetterSpeedModifier * 0.025f)) * Time.deltaTime);
            //transform.rotation = Quaternion.identity;
        } 

        if (gameObject.transform.position.y < -10)
        {
            gameObject.SetActive(false);
        }
    }

    public void PopThis()
    {
        //LetterAnimation.SetTrigger("Pop");
        //gameObject.SetActive(false);
      
        StartCoroutine(PopThisBubble());
    }

    public void PopBubble(GameObject PoppedBubble)
    {
        this.GetComponent<Collider2D>().enabled = false;
        Debug.Log("Popping Bubble");
        Destroy(PoppedBubble);
        PoppedBubble.SetActive(false);
        LetterAnimation.SetTrigger("Pop");
        gameObject.SetActive(false);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //bullet
        if (this.gameObject.name == "Bullet")
        {
         
           // RB2D.gravityScale = 0;
            if (collision.collider.name == "Letter" /*|| collision.collider.name == "Border"*/)
            {
                player.canShoot = true;
                if (collision.collider.tag == this.gameObject.tag)
                {
                    Debug.Log("Match hit");
                    LetterScript bubble = collision.collider.gameObject.GetComponent<LetterScript>();
                    bubble.PopThis();
                    PopThis();
                     //PopBubble(collision.collider.gameObject);
                    //StartCoroutine(popBulletBubble(collision.collider.gameObject));
                    MainScript.CurrentWord += LetterText;
                }
         
                //var speed = lastVelocity.magnitude;
                //var direction = Vector3.Reflect(lastVelocity.normalized, collision.contacts[0].normal);
         
                //RB2D.velocity = direction * Mathf.Max(speed, 1f);
                Debug.Log(LetterSpeed);
                gameObject.name = "Letter";
            }

            if (collision.collider.name == "Border")
            {
                Debug.Log("Border hit");
                Vector2 surfaceNormal = collision.contacts[0].normal;
                RB2D.velocity = Vector2.Reflect(lastVelocity, surfaceNormal);
            }
        }

        //letter
        if (this.gameObject.name == "Letter")
        {
            if (collision.collider.CompareTag(this.gameObject.tag))
            {
                MatchingLetters.Add(collision.gameObject);
            }
        }
    }

    
    private void DisableLetterMovement()
    {
        LetterSpeed = 0;
    }

    public void MoveTheBubble()
    {
        
        StartCoroutine(MoveBubble());
    }

    IEnumerator PopThisBubble()
    {
        LetterAnimation.SetTrigger("Pop");
        yield return new WaitForSeconds(0.9f);
        GameEvents.current.OnWrongWord -= MoveTheBubble;
        Destroy(this.gameObject);
        gameObject.SetActive(false);
    }

    IEnumerator MoveBubble()
    {
        LetterSpeedModifier = 3 ;
        yield return new WaitForSeconds(1.0f);

        LetterSpeedModifier = 0; 
    }
    //IEnumerator popBulletBubble(GameObject pop)
    //{
    //    Animator anim = pop.GetComponentInChildren<Animator>();

    //    anim.SetTrigger("Pop");
    //    yield return new WaitForSeconds(0.9f);
    //    pop.SetActive(false);
    //}
}

