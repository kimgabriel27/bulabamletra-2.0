using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridMember : MonoBehaviour
{
  //  public Grid 
    public GameObject parent;
    public int row;
    public int column;
    public string kind;
    public string state;

    public const float POP_SPEED = 0.9f;
    public const float EXPLODE_SPEED = 5f;
    public const float KILL_Y = -30f;
    public float speed = 0;
    Animator LetterAnimation;

    public GameObject childAnim;


    public Vector3 startPos;
    private Vector3 _v;
    public float dur = 1;
    public Vector3 differentialPos;

    private void Start()
    {
        LetterAnimation = childAnim.GetComponent<Animator>();
      //  GameEvents.current.OnLetterMove += GridMemberMove;
        //GameEvents.current.OnLevelReset += PopBubble;
        startPos = this.transform.position;
        differentialPos = new Vector3(0, 1.88f, 0);
    }
    public void Update()
    {
        if (state == "Pop")
        {
            Debug.Log(state);
            CircleCollider2D cc = GetComponent<CircleCollider2D>();
            if (cc != null)
                cc.enabled = false;

            GameEvents.current.OnLetterMove -= GridMemberMove;
            StartCoroutine(PopThisBubble());
            //transform.localScale = transform.localScale * POP_SPEED;
            //if (transform.localScale.sqrMagnitude < 0.05f)
            //{
            //    Destroy(gameObject);
            //    Debug.Log("Pop");
            //}
        }
        else if (state == "Explode")
        {
            Debug.Log(state + "|" + this.gameObject.name);
            CircleCollider2D cc = GetComponent<CircleCollider2D>();
            if (cc != null)
                cc.enabled = false;

            Rigidbody2D rb = GetComponent<Rigidbody2D>();
            if (rb != null)
            {
                rb.gravityScale = 1f;
                rb.velocity = new Vector3(
                    Random.Range(-EXPLODE_SPEED, EXPLODE_SPEED),
                    Random.Range(-EXPLODE_SPEED, EXPLODE_SPEED),
                    0f
                );
            }
            state = "Fall";
        }
        else if (state == "Fall")
        {
            if (transform.position.y < KILL_Y)
            {
                Debug.Log(state);
                GameEvents.current.OnLetterMove -= GridMemberMove;
                Destroy(gameObject);
                

            }
        }
      
    }
    void GridMemberMove()
    {
        StartCoroutine(MoveBubble());
    }

    void PopBubble()
    {
        StartCoroutine(PopThisBubble());
    }
    IEnumerator MoveBubble()
    {
        float time = 0;
        Vector3 targetPos = startPos - differentialPos;
        while (time < dur)
        {
            time += Time.deltaTime;
            transform.position = Vector3.Lerp(startPos, targetPos, time / dur);
            yield return null;
        }
        startPos = transform.position;  
    }
    IEnumerator PopThisBubble()
    {
        LetterAnimation.SetTrigger("Pop");
        yield return new WaitForSeconds(0.9f);
        // GameEvents.current.OnWrongWord -= MoveTheBubble;
        GameEvents.current.OnLevelReset -= PopBubble;
    
        Destroy(this.gameObject);
        gameObject.SetActive(false);
    }

}
