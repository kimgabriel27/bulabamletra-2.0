using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hitter : MonoBehaviour
{
    public string kind;
    public GameObject parent;
    public Sprite specialBubble;

    Animator LetterAnimation;

    public GameObject childAnim;
   
    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.name == "Deathborder")
        {
            Physics2D.IgnoreCollision(this.GetComponent<CircleCollider2D>(), collider);
            return;
        }
       
        if (collider != null)
        {
           if(collider.GetComponent<GridMember>())
            {
                CircleCollider2D selfcollider = GetComponent<CircleCollider2D>();
                if (selfcollider != null)
                {
                    selfcollider.enabled = false;
                }
                GridManager gridManager = parent.GetComponent<GridManager>();
                if (gridManager != null)
                {
                    GameObject newBubble = gridManager.Create(transform.position, kind);
                    if (newBubble != null)
                    {
                        Debug.Log("Create Grid Member: " + kind);
                        GridMember newGridMember = newBubble.GetComponent<GridMember>();
                        if (newGridMember != null)
                        {
                            gridManager.Seek(newGridMember.column, -newGridMember.row, newGridMember.kind);
                            Debug.Log("row:" + newGridMember.row + "/ " + "column:" + newGridMember.column);
                        }
                         
                        
                    }
                }
                //Launcher launcher = parent.GetComponent<Launcher>();
                //if (launcher != null)
                //{
                //    launcher.load = null;
                //    launcher.Load();
                //}
                MainGameScript.GetInstance.playerScript.Load();
                Destroy(gameObject);
            }
           // Debug.Log(kind);
          
        }
    }

  

}
