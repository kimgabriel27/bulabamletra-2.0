using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bubble : MonoBehaviour
{
    public float raycastRange;
    public float raycastOffset;

    public bool isFixed;
    public bool isConnected;
    public bool isDeletingSelf = false;
    public BubbleLetter bubbleLetter;
    public Animator anim;
    GameManager gameManager;
    public GameObject childAnim;



    private void Start()
    {
        gameManager = GameManager.instance;
        anim = childAnim.GetComponent<Animator>();
    }

    private void Update()
    {
        if (!isFixed)
        {
            gameObject.layer = LayerMask.NameToLayer("Ignore Raycast");
        }
        else
        {
            gameObject.layer = LayerMask.NameToLayer("Default");
        }
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
 
        if (collision.gameObject.tag == "Bubble" && collision.gameObject.GetComponent<Bubble>().isFixed)
        {
           // Debug.Log("Collided");
            if (!isFixed)
            {
                HasCollided();

            }
        }

        if (collision.gameObject.tag == "Limit")
        {
            if (!isFixed)
            {
                HasCollided();
            }
        }
     
    }

    

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "DeathBorder" && isFixed)
        {
            Debug.Log("Bubble Touch");
            gameManager.ShowLoseScreen();
            gameManager.blackBG.SetActive(true);
           
        }
    }

    private void HasCollided()
    {
        var rb = GetComponent<Rigidbody2D>();
      
        Destroy(rb);
        transform.rotation = Quaternion.identity;
        isFixed = true;
        LevelManager.instance.SetAsBubbleAreaChild(transform);
        GameManager.instance.ProcessTurn(transform);
     
    }

    public List<Transform> GetNeighbors()
    {
        List<RaycastHit2D> hits = new List<RaycastHit2D>();
        List<Transform> neighbors = new List<Transform>();

        
        hits.Add(Physics2D.Raycast(new Vector2(transform.position.x - raycastOffset, transform.position.y), Vector3.left, raycastRange));
        hits.Add(Physics2D.Raycast(new Vector2(transform.position.x + raycastOffset, transform.position.y), Vector3.right, raycastRange));
        hits.Add(Physics2D.Raycast(new Vector2(transform.position.x - raycastOffset, transform.position.y + raycastOffset), new Vector2(-1f, 1f), raycastRange));
        hits.Add(Physics2D.Raycast(new Vector2(transform.position.x - raycastOffset, transform.position.y - raycastOffset), new Vector2(-1f, -1f), raycastRange));
        hits.Add(Physics2D.Raycast(new Vector2(transform.position.x + raycastOffset, transform.position.y + raycastOffset), new Vector2(1f, 1f), raycastRange));
        hits.Add(Physics2D.Raycast(new Vector2(transform.position.x + raycastOffset, transform.position.y - raycastOffset), new Vector2(1f, -1f), raycastRange));

        foreach (RaycastHit2D hit in hits)
        {
            if (hit.collider != null && hit.transform.tag.Equals("Bubble") && !isDeletingSelf)
            {
              //  Debug.Log(hit.transform.gameObject.name);
                neighbors.Add(hit.transform);
            }
        }

        return neighbors;
    }

    void OnBecameInvisible()
    {
        Destroy(gameObject);
    }
    
    public float BubbleDestroy()
    {
        isDeletingSelf = true;
        StartCoroutine(WaitSeconds(0.5f));
        return anim.GetCurrentAnimatorStateInfo(0).length;

    }

    public void  DroppingDestroy()
    {
        isDeletingSelf = true;
        StartCoroutine(WaitSeconds(1f));
    }


    public enum BubbleLetter
    {
        A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y,Z
    }

    IEnumerator WaitSeconds(float seconds)
    {
        anim.SetTrigger("Pop");
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(seconds);
        Destroy(this.gameObject);
    }
}
