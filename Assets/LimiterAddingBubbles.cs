using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LimiterAddingBubbles : MonoBehaviour
{

    LevelManager levelManager;

    private void Start()
    {
        levelManager = LevelManager.instance;
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if(collision.gameObject.GetComponent<Bubble>() && collision.gameObject.gameObject.GetComponent<Bubble>().isFixed)
        {
            levelManager.AddNewLine();
        }
    }
}
